import React, { useState } from 'react';
import { Text, Image, TouchableOpacity } from 'react-native';
import { Colors } from '../assets/colors'

function Drawing({ item, disabled, onPress, backgroundColor  }) {
    const lastImageIndex = item.img.length - 1;

    return (
        <TouchableOpacity 
            disabled={disabled}
            onPress={onPress}
            style={{
                width: '45%',
                height: 200,
                backgroundColor,
                marginTop: 30,
                borderRadius: 25,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <Image
                source={item.img[lastImageIndex]}
                style={{
                    width: '90%',
                    height: '90%',
                    backgroundColor: '#EEEEEE',
                    borderRadius: 18,
                }}
            />
        </TouchableOpacity>
    );
}

export default Drawing;