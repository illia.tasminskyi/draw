import * as React from 'react';
import { Text as TextReact, View } from 'react-native';

function Text({text, fs = 16, spanText = false, spanColor}) {
  if (spanText === false) {
    return (
      <TextReact style={{ fontSize: fs, color: '#FFFFFF' }}>{text}</TextReact>
    );
  } else {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <TextReact style={{ fontSize: fs, color: spanColor }}>{spanText}</TextReact>
        <TextReact style={{ fontSize: fs, color: '#FFFFFF' }}>
          {text}
        </TextReact>
      </View>
    );
  }
}

export default Text;