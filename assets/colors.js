// colors.js

export const Colors = {
    c1: '#28EAE6',
    c2: '#D5B9FF',
    c3: '#FFADAE',
    c4: '#EDC9AA',
    bg: '#1F1F23',
    white: '#FFFFFF',
  };
  