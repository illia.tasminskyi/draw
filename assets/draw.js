
export const Draw = [
    {
        name: 'Butterfly',
        tags: 'butterfly,butterfly2,butterfly3',
        img: [
            require('./butterfly/butterfly_1.png'),
            require('./butterfly/butterfly_2.png'),
            require('./butterfly/butterfly_3.png'),
            require('./butterfly/butterfly_4.png'),
            require('./butterfly/butterfly_5.png'),
            require('./butterfly/butterfly_6.png'),
            require('./butterfly/butterfly_7.png'),
        ]
    },
    {
        name: 'Moon',
        tags: 'moon,moon2,moon3',
        img: [
            require('./moon/moon_1.png'),
            require('./moon/moon_2.png'),
            require('./moon/moon_3.png'),
            require('./moon/moon_4.png'),
        ]
    }
];
