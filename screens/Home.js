import React, { useEffect, useState, useMemo } from 'react';
import { Button, View, Image, SafeAreaView, ScrollView } from 'react-native';
// import { InterstitialAd, AdEventType, TestIds } from 'react-native-google-mobile-ads';
import Text from '../components/Text'
import Drawing from '../components/Drawing';
import { Colors } from '../assets/colors'
import { Draw } from '../assets/draw'

const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
const colorArr = [Colors.c1, Colors.c2, Colors.c3, Colors.c4];

// const adUnitId = __DEV__ ? TestIds.INTERSTITIAL : 'ca-app-pub-xxxxxxxxxxxxx/yyyyyyyyyyyyyy';

// const interstitial = InterstitialAd.createForAdRequest(adUnitId, {
//   requestNonPersonalizedAdsOnly: true,
//   keywords: ['fashion', 'clothing'],
// });

function Home({ navigation }) {
  const [loaded, setLoaded] = useState(false);

  // useEffect(() => {
  //   const unsubscribe = interstitial.addAdEventListener(AdEventType.LOADED, () => {
  //     setLoaded(true);
  //   });

  //   interstitial.addAdEventListener(AdEventType.CLOSED, () => {
  //     setLoaded(false);
  //     interstitial.load();
  //     // navigation.navigate('Slider', { data: 'ttt' });
  //   });

  //   interstitial.load();

  //   return unsubscribe;
  // }, []);

  if (!loaded) {
    return null;
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.bg }}>
      <View style={{ flexDirection: "row", justifyContent: 'center',  paddingBottom: 20 }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30 }}>
            <Text text="Hi, friend!" fs={33} />
            <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
                <Text text="Let's go draw" fs={20} />
                <View style={{ paddingLeft: 10 }}>
                  <Image
                    source={require('../assets/icon/emoticon.png')}
                  />
                </View>
            </View>
        </View>
      </View>
      <SafeAreaView>
        <ScrollView contentContainerStyle={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap', paddingBottom: 140 }}>
          {Draw.map((item, index) => (
            <Drawing
              item={item}
              key={index}
              disabled={!loaded}
              onPress={() => {
                if (loaded) {
                  interstitial.show();
                  navigation.navigate('Slider', { data: item });
                }
              }}
              backgroundColor={ colorArr[index % 4] }
            />
          ))}
        </ScrollView>
    </SafeAreaView>
    </View>
  );
}

export default Home;