import * as React from 'react';
import { TouchableOpacity, View, Image, Animated, PanResponder } from 'react-native';
import Text from '../components/Text'
import { Colors } from '../assets/colors'

function Slider({ navigation, route }) {
  const [full, setFull] = React.useState(false);
  const [imgCount, setImgCount] = React.useState(0)
  const { data } = route.params;

  const tags = data.tags.split(',');
  const colors = [Colors.c1, Colors.c2, Colors.c3, Colors.c4];

  const panResponder = React.useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: (_, gestureState) => {
        // You can handle gestures here if needed
      },
      onPanResponderRelease: (_, gestureState) => {
        const { dx } = gestureState;
        // Check if the swipe is to the right (positive dx)
        if (dx > 50) {
          setImgCount((prevImgCount) => Math.max(0, prevImgCount - 1));
        }
        // Check if the swipe is to the left (negative dx)
        else if (dx < -50) {
          setImgCount((prevImgCount) =>
            Math.min(data.img.length - 1, prevImgCount + 1)
          );
        }
      },
    })
  ).current;

	const PictureBack = () => {
		if (imgCount > 0) setImgCount(imgCount - 1)
	}

	const PictureForward = () => {
		if (imgCount < data.img.length - 1) setImgCount(imgCount + 1)
	}

  return (
    <View {...panResponder.panHandlers}
      style={{ 
        flex: 1, 
        backgroundColor: Colors.bg, 
        paddingTop: 10, 
      }}
    >
      {full === false ? (
        <>
          <TouchableOpacity
            style={{ marginLeft: 20 }}
            onPress={() => navigation.navigate('Home')}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center'}}>
              <Image 
                source={require('../assets/icon/close.png')}
                style={{ marginRight: 5 }} />
              <Text text="Close" />
            </View>
          </TouchableOpacity>
          <View style={{ 
            justifyContent: 'space-between', 
            alignItems: 'center',
            marginTop: 20
          }}>
            <Image 
              source={data.img[imgCount]}
              style={{
                width: '90%',
                height: '80%',
                backgroundColor: Colors.white,
                borderRadius: 20,
              }}
            />
            <Text text={data.name} />
            <View style={{ width: '70%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>
              {
                tags.map((item, index) => (
                  <Text key={index} spanText='#' spanColor={colors[index]} text={item} />
                ))
              }
            </View>
            <View style={{ width: '50%', flexDirection: 'row', justifyContent: 'space-around'}}>
              <TouchableOpacity 
                onPress={() => PictureBack()}
                style={{ 
                  flexDirection: 'row', 
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: Colors.c1,
                  width: 50,
                  height: 50,
                  borderRadius: 10
                }}
              >
                <Image 
                  source={require('../assets/icon/angle-left.png')} 
                  style={{ marginRight: 5 }} />
              </TouchableOpacity>
              <TouchableOpacity 
                onPress={() => setFull(true)}
                style={{ 
                  flexDirection: 'row', 
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: Colors.c2,
                  width: 50,
                  height: 50,
                  borderRadius: 10,
                }}
              >
                <Image source={require('../assets/icon/search.png')} />
              </TouchableOpacity>
              <TouchableOpacity 
                onPress={() => PictureForward()}
                style={{ 
                  flexDirection: 'row', 
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: Colors.c3,
                  width: 50,
                  height: 50,
                  borderRadius: 10
                }}
              >
                <Image 
                  source={require('../assets/icon/angle-right.png')} />
              </TouchableOpacity>
            </View>
          </View>
        </>
      ) : (
        <>
          <TouchableOpacity
            onPress={() => setFull(false)}
            style={{ position: 'absolute', right: 20, top: 30, zIndex: 20 }} 
          >
            <Image 
              source={require('../assets/icon/cross.png')}
            />
          </TouchableOpacity>
          <Image 
            source={data.img[imgCount]}
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: Colors.white,
              borderRadius: 20,
            }}
          />
        </>
      )}
    </View>
  );
}

export default Slider;