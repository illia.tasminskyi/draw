import * as React from 'react';
import 'expo-dev-client';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './screens/Home'
import Slider from './screens/Slider'
import { SafeAreaView } from 'react-native-safe-area-context';
import { Colors } from './assets/colors'

const Stack = createNativeStackNavigator();

function App() {
  return (
    <SafeAreaView style={{ 
      flex: 1, 
      backgroundColor: Colors.bg,
      padding: 20,
    }}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen 
            options={{ headerShown: false }} 
            name="Home" 
            component={Home} 
            />
          <Stack.Screen
            options={{ headerShown: false }} 
            name="Slider" 
            component={Slider} 
            />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
}

export default App;